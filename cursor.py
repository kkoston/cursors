# -*- coding: utf-8 -*-

import time
import sys

def swrite(s):
    sys.stdout.write(s)
    
class Cursor:
    ctl_str = ""
    
    cur_ctl = {
        "up":       "\033[%dA",
        "down":     "\033[%dB",
        "right":    "\033[%dC",
        "left":     "\033[%dD",
        "pos":      "\033[%d;%dH",
        "save":     "\033[7",
        "restore":  "\033[8",                    
    }
    
    def action(self, *args):
        swrite(self.ctl_str % args)
            
    def __getattr__(self, key):
        self.ctl_str = self.cur_ctl[key]
        return self.action
        
# -- test --

class Window:
   def  __init__(self, left, top, width, height):
        cur = Cursor()
        cur.pos(top, left)
        swrite("┌" + "─"*width + "┐")
        for r in range(height):
            cur.pos(top + r + 1, left)
            swrite("┌" + " "*width + "┐")


win = Window(40, 10, 10, 5)



